# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
  config.vm.provider "virtualbox"
  config.vm.provision "shell", inline: <<-SHELL
  # my public key
     echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD1j1A0g7ebdghBy2UX+baArSx9jzYGTS5HwqyKEniRZ49wc/P20v5HLboCW+pzf3W8GYN3wAN2yTgxczydBO30bXAFhmAwsv/WI43kPRHLlgswSTV7KZcChqc//IsLi74LDY2kxpxcfQZG4uD3ZijsATbyZYO1atBcG5o2Ufobu0FQpSAh9F7eR0mtXzMa2XA2Mt8OlWkzmM3Oz/ORdAPqt9AMLIDk5jiCsBF8qEkLvDHtgU2MwWE6JywbdEYwQ+c6cB17LnJHaoNjOnQ/6z/YLV0TBnXeDO+SwW5dm36IPC9bulBqy4q/mo9cV3Ml3qOT41HsA2fRG8FxXpMum5aD OpenSSH-rsa-import-101417" >> /home/vagrant/.ssh/authorized_keys
  # ansible public key
     echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC2+wYYwuVAWBI5W4pQdjPDi5qPCUE2OqklYEUQ3yyHP3AxBAyzUdLnq2sXphYG5j0ygovGgp+yMpERTVzg0ZryfoL4M6CFZKNgoT7nnyklcmNxggoEsZsVBW/2P+0U3ikpybQy0ZqpQkPs6LcA+y4HPjVoy7d4q+03Wov2eUVmNCLXIeT9AN3qs7BEsKc46G44by9J3JITeFFu3rRDtUcuhzEc/uhGyCdP7Ma49e1oyPkhqtOHC1ykNGbfREvGvyvJR3jeK/2wlMW+vq/8sODg0iMTpdwE9SNvhkZlYPMiRWTYBAUEXcX3Urwws8aogc0ILbV5WNY/ka8nepHZKRoX vagrant@admin" >> /home/vagrant/.ssh/authorized_keys
  SHELL

  config.vm.define "nginx1" do |nginx1|
    nginx1.vm.network "private_network", ip: "192.168.50.10"
    nginx1.vm.network 'forwarded_port', guest: 80, host: 8080
    nginx1.vm.hostname = "nginx1"
    nginx1.vm.provision "shell", inline: <<-SHELL
      setenforce 0
    SHELL
  end

  config.vm.define "nginx2" do |nginx2|
    nginx2.vm.network "private_network", ip: "192.168.50.9"
    nginx2.vm.network 'forwarded_port', guest: 80, host: 8081
    nginx2.vm.hostname = "nginx2"
    nginx2.vm.provision "shell", inline: <<-SHELL
      setenforce 0
    SHELL
  end

  config.vm.define "mysql1" do |mysql1|
    mysql1.vm.network "private_network", ip: "192.168.50.12"
    mysql1.vm.hostname = "mysql1"
    mysql1.vm.synced_folder "roles/mariadb_startup/files/", "/opt/", type: "rsync"
    mysql1.vm.provision "shell", inline: <<-SHELL
      setenforce 0
      sed -i 's/^SELINUX=.*/SELINUX=disabled/g' /etc/selinux/config
    SHELL
  end

  config.vm.define "mysql2" do |mysql2|
    mysql2.vm.network "private_network", ip: "192.168.50.13"
    mysql2.vm.hostname = "mysql2"
    mysql2.vm.provision "shell", inline: <<-SHELL
      setenforce 0
      sed -i 's/^SELINUX=.*/SELINUX=disabled/g' /etc/selinux/config
    SHELL
  end

  config.vm.define "mysql3" do |mysql3|
    mysql3.vm.network "private_network", ip: "192.168.50.14"
    mysql3.vm.hostname = "mysql3"
    mysql3.vm.provision "shell", inline: <<-SHELL
      setenforce 0
      sed -i 's/^SELINUX=.*/SELINUX=disabled/g' /etc/selinux/config
    SHELL
  end

  config.vm.define "ceph1" do |ceph1|
    ceph1.vm.network "private_network", ip: "192.168.50.20"
    ceph1.vm.hostname = "ceph1"
    ceph1.vm.provider "virtualbox" do |v|
          second_disk = "ceph-disk1.vmdk"
          v.customize ['createhd', '--filename', second_disk, '--size', 10 * 1024]
          v.customize [ "storageattach", :id , "--storagectl", "IDE", "--port", "1", "--device", "0", "--type", "hdd", "--medium", second_disk]
    end
    ceph1.vm.provision "shell", inline: <<-SHELL
      setenforce 0
      sed -i 's/^SELINUX=.*/SELINUX=disabled/g' /etc/selinux/config
    SHELL
  end

  config.vm.define "ceph2" do |ceph2|
    ceph2.vm.network "private_network", ip: "192.168.50.21"
    ceph2.vm.hostname = "ceph2"
    ceph2.vm.provider "virtualbox" do |v|
          second_disk = "ceph-disk2.vmdk"
          v.customize ['createhd', '--filename', second_disk, '--size', 10 * 1024]
          v.customize [ "storageattach", :id , "--storagectl", "IDE", "--port", "1", "--device", "0", "--type", "hdd", "--medium", second_disk]
    end
    ceph2.vm.provision "shell", inline: <<-SHELL
      setenforce 0
      sed -i 's/^SELINUX=.*/SELINUX=disabled/g' /etc/selinux/config
    SHELL
  end

  config.vm.define "ceph3" do |ceph3|
    ceph3.vm.network "private_network", ip: "192.168.50.22"
    ceph3.vm.hostname = "ceph3"
    ceph3.vm.provider "virtualbox" do |v|
          second_disk = "ceph-disk3.vmdk"
          v.customize ['createhd', '--filename', second_disk, '--size', 10 * 1024]
          v.customize [ "storageattach", :id , "--storagectl", "IDE", "--port", "1", "--device", "0", "--type", "hdd", "--medium", second_disk]
    end
    ceph3.vm.provision "shell", inline: <<-SHELL
      setenforce 0
      sed -i 's/^SELINUX=.*/SELINUX=disabled/g' /etc/selinux/config
    SHELL
  end

  config.vm.define "admin" do |admin|
    admin.vm.network "private_network", ip: "192.168.50.11"
    admin.vm.network 'forwarded_port', guest: 3000, host: 3000, host_ip: '127.0.0.1'
    admin.vm.hostname = "admin"
    admin.vm.provision "file", source: "id_rsa", destination: "/home/vagrant/.ssh/id_rsa"

    # for windows
    # admin.vm.synced_folder "roles/", "/opt/roles"
    # for linux
    admin.vm.synced_folder "roles/", "/opt/roles", type: "rsync"

    admin.vm.provision "shell", inline: <<-SHELL
      setenforce 0
      sed -i 's/^SELINUX=.*/SELINUX=disabled/g' /etc/selinux/config
      yum install ansible -y
      chmod 600 /home/vagrant/.ssh/id_rsa
      sed -i "12i host_key_checking = False" /etc/ansible/ansible.cfg
    SHELL
    admin.vm.provision "shell", privileged: false, inline: <<-SHELL
      ansible-playbook -i /opt/roles/hosts -b /opt/roles/playbook.yml -vvv
    SHELL
  end
end
