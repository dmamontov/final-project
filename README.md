# final-project

Стенд состоит из 9 нод:

1) admin. 192.168.50.11
Включает в себя: ansible, grafana, telegraf, influx

2) nginx1. 192.168.50.10
Включает в себя: nginx, php-fpm, proxy_sql, telegraf
3) nginx2. 192.168.50.9
Включает в себя: nginx, php-fpm, proxy_sql, telegraf

4) mysql1. 192.168.50.12
Включает в себя: mysql, telegraf
5) mysql2. 192.168.50.13
Включает в себя: mysql, telegraf
6) mysql3. 192.168.50.14
Включает в себя: mysql, telegraf

7) ceph1. 192.168.50.20 хранилище ceph
8) cehph2. 192.168.50.21 хранилище ceph
9) ceph3. 192.168.50.22 хранилище ceph

Все компоненты площадки ставртуют при vagrant up, кроме ceph. Его надо додеплоить руками
```
На админ ноде:
su ceph && cd

Деплоим кластер
ceph-deploy new ceph1 ceph2 ceph3
ceph-deploy install ceph1 ceph2 ceph3 nginx1 nginx2
ceph-deploy mon create-initial
ceph-deploy admin ceph1 ceph2 ceph3 nginx1 nginx2
ceph-deploy mgr create ceph1 ceph2 ceph3

Добавляем три OSD демона
ceph-deploy osd create --data /dev/sdb ceph1
ceph-deploy osd create --data /dev/sdb ceph2
ceph-deploy osd create --data /dev/sdb ceph3

Кластер поднят
```

Подключение клиента:
```
on client (nginx1)

ceph osd pool create otushd 250 250
ceph osd lspools

rbd create otusrbd --size 2048 -p otushd #-m ceph1
rbd feature disable otusrbd -p otushd deep-flatten,fast-diff,object-map

on client (nginx1\2)

rbd map otusrbd --pool otushd #--name client.admin -m ceph1
mkfs.xfs /dev/rbd0
mount /dev/rbd0 /mnt/
lsblk

копируем данные на сеф вольюм
cp /usr/share/nginx/html/index.php /mnt/
cp /usr/share/nginx/html/kotik.jpg /mnt/
```

ПОЛЕЗНЫЕ КОМАНДЫ

```
лог прокси proxy_sql
less /var/lib/proxysql/proxysql.log

коннект
mysql -u admin -padmin -h 127.0.0.1 -P6032
ноды
SELECT * FROM mysql_servers;

вставить выбитый сервер
INSERT INTO mysql_servers(hostgroup_id,hostname,port) VALUES (1,'192.168.50.11',3306);
Query OK, 1 row affected (0.01 sec)

статус кластера
SHOW GLOBAL STATUS LIKE 'wsrep_%';

если кластер разъехался
SET GLOBAL wsrep_provider_options='pc.bootstrap=YES';

запрос
use FruitShop
select * from Units

подключение к инфлюксу
http://172.17.0.3:8086
telegraf

```
